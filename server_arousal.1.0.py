'''
AROUSAL SYSTEM

Current state: http://127.0.0.1:5010/current_state
Add chemical: http://127.0.0.1:5000/add?chemical=noradrenaline&value=100

'''

from flask import Flask,request,jsonify
from flask_cors import CORS,cross_origin
import os
import pprint
import numpy as np
pp = pprint.PrettyPrinter(indent=4)
from functions import *

# =====================================================================================

state = {"noradrenaline": {"timestamp": datetime.now(), "value": 0}}
exp_decay = 0.2

# =====================================================================================

app=Flask(__name__)
CORS(app, support_credentials=True)
@cross_origin(supports_credentials=True)

# =====================================================================================

@app.route('/', methods=['GET'])
def index():
    return "Arousal server running!"

# =====================================================================================

def update_state():

    for key in state:

        # Difference in time since last poll
        diff_time = (datetime.now() - state[key]["timestamp"]).total_seconds()

        # Calculate new value (apply decay)
        decay_factor = np.exp(-1 * diff_time * exp_decay)
        new_val = round(state[key]["value"] * decay_factor,5)

        # Update state
        state[key]["value"] = new_val
        state[key]["timestamp"] = datetime.now()

# =====================================================================================

@app.route("/current_state", methods=['GET'])
def current_state():

    #log("Arousal System: /current_state")

    update_state()

    return return_json(state)

# =====================================================================================

@app.route("/add", methods=['GET'])
def add(chemical="", value=0):

    log("Arousal System: /add")

    get_chemical = request.args.get('chemical')
    get_value = round(float(request.args.get('value')))

    # Register Chemical if it does not exist
    if get_chemical not in [i for i in state]:
        state[get_chemical] = {"timestamp": 0, "value": 0}

    update_state()

    # Update the chemical state
    state[get_chemical]["value"] = min(state[get_chemical]["value"] + get_value, 100)
    state[get_chemical]["timestamp"] = datetime.now()

    return return_json(state)

# =====================================================================================

@app.route("/cls")
def cls():
    os.system('cls')
    return ""

# =====================================================================================

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()

@app.route('/shutdown')
def shutdown():
    shutdown_server()
    return 'Server shutting down...'

# =====================================================================================

cls()
log ("Starting Arousal System: %s" % str(datetime.now()), level="INFO", color="WARN")
app.run(host=host, port=ports["arousal"], debug=True, threaded=True)

# =====================================================================================

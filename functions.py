# ==============================================================================
# Common functions
# ==============================================================================

import numpy as np
np.random.seed(0)
import pickle
import json
import cv2
import requests
from datetime import datetime, timedelta
from colorama import init; init(); from colorama import Fore, Back, Style
import matplotlib.pyplot as plt
import base64
import io
import glob

from SANN import *

import os
os.environ["KERAS_BACKEND"] = "theano"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
from keras.layers import Input, Dense
from keras.models import Sequential, Model
from keras.datasets import fashion_mnist, mnist
from keras.models import model_from_json
from keras.optimizers import Adam
from keras.utils import to_categorical
from keras.callbacks import ModelCheckpoint, EarlyStopping

import logging
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

import warnings
warnings.simplefilter(action='ignore')

# ==============================================================================

host = '127.0.0.1'

ports = {
    "arousal":  5000,
    "thalamus": 5010,
    "cortex":   5020,
}

# ==============================================================================

def nothing(x): pass

# ==============================================================================

def b64_encode(img_array=[]):
    img_b64 = base64.urlsafe_b64encode(np.round(img_array,2))
    img_b64_decoded = img_b64.decode('utf-8')
    return img_b64_decoded

# ==============================================================================

def b64_decode(str_b64_encoded=""):
    img_b64_encoded = str_b64_encoded.encode('utf-8')
    return np.frombuffer(base64.urlsafe_b64decode(img_b64_encoded), np.float32)

# ==============================================================================

def empty_img():
    return b64_encode(np.zeros((28,28), dtype=np.float32))

# ==============================================================================

def log(message, level="info", color=""):
    message_clean = str(message).strip()
    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    colour_codes = {
    "INFO": Fore.WHITE,
    "ERR.": Fore.RED,
    "WARN": Fore.YELLOW,
    "PASS": Fore.GREEN,
    "BLUE": Fore.BLUE}
    print_str = Fore.WHITE + ("%s" % timestamp)
    if len(color) == 0:
        color = level
    print_str += " [" + colour_codes[color.upper()] + ("%4s" % level.upper()) + Fore.WHITE + "]"
    print_str += Fore.WHITE + ": " + colour_codes[color.upper()] + ("%s" % message_clean) + Fore.WHITE
    print(print_str)

# ==============================================================================

def cleanup_log_array(arr, secs=5):
    return [i for i in arr if i[0] >= (datetime.now()-timedelta(seconds=secs))]

# ==============================================================================

def get_current_state(port_name="arousal"):
    response = requests.get("http://%s:%s/current_state" % (host, ports[port_name]))
    return response.json()

# ==============================================================================

def send_to(port_name="cortex", img=empty_img()):
    img_b64_decoded = b64_encode(img)
    response = requests.get("http://%s:%s/process_image?img_b64_decoded=%s" % (host, ports[port_name], img_b64_decoded))
    state = response.json()
    return state

# ==============================================================================

def trigger_chemical_release(chemical="noradrenaline", value=0):
    requests.get("http://%s:%s/add?chemical=%s&value=%s" % (host, ports["arousal"], chemical, value))
    return True

# ==============================================================================

def show_image(img):
    plt.imshow(img, cmap='Greys_r')
    plt.axis('off')
    plt.show()

# ==============================================================================

def gui_bars(num, max_bars=100, bar_width=90):
    max_bars = max(num, max_bars)
    percent = num / max_bars
    bars = int(np.ceil(percent * bar_width))
    output_str = "[" + ("|" * bars) + (" "*(bar_width-bars)) + "]"
    return output_str

# ==============================================================================

def rmsdiff(im1, im2):
    """Calculates the root mean square error (RSME) between two images"""
    return np.sqrt(np.mean((im1-im2)**2))

# ==============================================================================

def load_data():

    (x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()

    # Normalize all values between 0 and 1
    x_train = x_train.astype('float32') / 255.
    x_test = x_test.astype('float32') / 255.

    # Flatten the 28x28 images into vectors of size 784
    x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
    x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

    return x_train, x_test

# ==============================================================================

def init_nn(input_dim=784, encoding_dim=16):

    autoencoder = Sequential()
    autoencoder.add(Dense(512, activation='elu', input_shape=(input_dim,)))
    autoencoder.add(Dense(encoding_dim, activation='elu', name="encoded"))
    autoencoder.add(Dense(256, activation='elu'))
    autoencoder.add(Dense(input_dim, activation='sigmoid'))
    autoencoder.compile(loss='mean_squared_error', optimizer = Adam(), metrics=['accuracy'])

    # Encoder Model
    encoder = Model(autoencoder.input, autoencoder.get_layer('encoded').output)

    # Decoder Model
    encoded_input = Input(shape=(encoding_dim,))
    decoder = autoencoder.layers[-2](encoded_input)
    decoder = autoencoder.layers[-1](decoder)
    decoder = Model(encoded_input, decoder)

    encoder._make_predict_function()
    decoder._make_predict_function()

    return autoencoder, encoder, decoder

# ==============================================================================

def train_nn(autoencoder, x_train, x_test, epochs_n=20):

    # Save H5 files at checkpoints
    #filepath="saved_nn_models\\weights-{epoch:02d}-{val_loss:.4f}.h5"
    #checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=0, save_best_only=True, mode='min')
    #callbacks_list = [checkpoint]

    # Early Stopping
    callbacks_list = [EarlyStopping(monitor = 'val_loss', min_delta = 0, patience = 10, verbose = True, mode = 'auto')]

    history = autoencoder.fit(  x_train,
                                x_train,
                                epochs=epochs_n,
                                batch_size=1,
                                shuffle=True,
                                verbose=1,
                                validation_data=(x_test, x_test),
                                callbacks=callbacks_list)

    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model accuracy')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['training data loss', 'testing data loss', 'testing RMSE'], loc='upper left')
    plt.show()

    return autoencoder, history

# ==============================================================================

def reconstruct(encoder, decoder, x_test):

    encoded_imgs = encoder.predict(x_test)
    decoded_imgs = decoder.predict(encoded_imgs)

    return encoded_imgs, decoded_imgs

# ==============================================================================

def reshape_resize_reshape(img, input_dim=28, output_dim=16):

    img_2D = img.reshape((input_dim,input_dim))
    img_16x16_2D = cv2.resize(img, (output_dim,output_dim))
    img_16x16_1D = img_16x16_2D.reshape((output_dim * output_dim))

    return img_16x16_1D

# ==============================================================================

def classify_from_encoding(sann, img_small):

    img_solve = sann.solve(np.array([img_small]))
    img_class = np.argmax(img_solve)
    img_conf = img_solve[0][img_class]

    reverse_salience_value = sann.calculate_reverse_salience(img_small.reshape(4*4))

    return img_class, img_conf, reverse_salience_value

# ==============================================================================

def lookup_label(class_key):

    labels = [
        "bird",
        "cat",
        "dog"
    ]

    try:
        class_key = min(len(labels), max(0,int(class_key)))
        label = labels[class_key]
    except:
        label = "None"

    return label

# ==============================================================================

def save_nn_to_file(model, name="nn"):

    # Save model
    json_filename = "saved_nn_models\\model_" + name + ".json"
    with open(json_filename, "w") as json_file:
        json_file.write(model.to_json())
    json_file.close()

    # Save weights
    h5_filename = "saved_nn_models\\model_" + name + ".h5"
    model.save_weights(h5_filename)

    return True

# ==============================================================================

def load_nn_from_file(model, name="nn"):

    # Load JSON
    '''
    json_filename = "saved_nn_models\\model_" + name + ".json"
    with open(json_filename) as file_handle:
        model = model_from_json(file_handle.read())
    '''

    # Load weights
    h5_filename = "saved_nn_models\\model_" + name + ".h5"
    model.load_weights(h5_filename)

    return model

# ==============================================================================

# define a function which returns an image as numpy array from figure
# https://stackoverflow.com/questions/7821518/matplotlib-save-plot-to-numpy-array

def get_img_from_fig(fig, dpi=180):
    buf = io.BytesIO()
    fig.savefig(buf, format="png", dpi=dpi)
    buf.seek(0)
    img_arr = np.frombuffer(buf.getvalue(), dtype=np.uint8)
    buf.close()
    img = cv2.imdecode(img_arr, 1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return img

# ==============================================================================

def open_image(filename):

    # Opens the file
    if not(os.path.isfile(filename)):
        return False, None # Exception if file does not exist
    try:
        img = cv2.imread(filename)
    except:
        return False, None # Exception if file cannot open
    return True, img

# ==============================================================================

def load_training_images(training_dir="dataset/*/*.*", img_dim=28, log_polar_transform=False, log_polar_res=5.0):

    # Training images
    images = []
    labels = []
    filename_array = glob.glob(training_dir)

    for filename in filename_array:
        status, new_file = open_image(filename)
        img = grayscale(new_file)
        img = cv2.resize(img, (img_dim, img_dim))
        images.append(img)
        label = filename.split("\\")[-2]
        labels.append(label)

    images = np.array(images)
    labels = to_categorical(labels)

    # Normalize all values between 0 and 1
    images = images.astype('float32') / 255.

    # Inverse Images
    images = 1 - images

    # Transform (log polar)
    if (log_polar_transform == True):
        images = transform_dataset(images, 0, 1, log_polar_res=log_polar_res)

    # Flatten the 28x28 images into vectors of size 784
    images_flat = images.reshape((len(images), np.prod(images.shape[1:])))

    return images_flat, labels

# ==============================================================================

def grayscale(img_original):

    img = img_original.copy()

    # Converts colour image to grayscale
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return img

# ==============================================================================

def transform_dataset(data, rotate_degrees=0, resize_percent=1, log_polar_res=5.0):

    x_data = []

    for i in range(0, len(data)):

        i_data = transform_logpolar(data[i], log_polar_res=log_polar_res)
        x_data.append(i_data)

    return np.array(x_data)

# ==============================================================================

def transform_logpolar(img, log_polar_res=5.0):

    value = np.sqrt(((img.shape[1]/log_polar_res)**2.0)+((img.shape[0]/log_polar_res)**2.0))
    log_polar_image = cv2.logPolar(img,(img.shape[1]/2, img.shape[0]/2), value, cv2.WARP_FILL_OUTLIERS)
    return log_polar_image

# ==============================================================================

def transform_logpolar_inv(img, log_polar_res=5.0):

    value = np.sqrt(((img.shape[1]/log_polar_res)**2.0)+((img.shape[0]/log_polar_res)**2.0))
    inv_log_polar_image = cv2.logPolar(img, (img.shape[1]/2, img.shape[0]/2), value, cv2.WARP_FILL_OUTLIERS + cv2.WARP_INVERSE_MAP)
    return inv_log_polar_image

# =====================================================================================

def datetimeconverter(o):
    if isinstance(o, datetime):
        return o.__str__()

# =====================================================================================

def return_json(dict_to_return):
    return json.dumps(dict_to_return, default=datetimeconverter)

# =====================================================================================

'''
CORTEX SYSTEM

Current state:          http://127.0.0.1:5020/current_state
Process image:          http://127.0.0.1:5020/process_image?img_b64_decoded=""
Load Autoencoder:       http://127.0.0.1:5020/load_autoencoder
Train Autoencoder:      http://127.0.0.1:5020/train_autoencoder
Evaluate Autoencoder:   http://127.0.0.1:5020/evaluate_autoencoder
Load SANN:              http://127.0.0.1:5020/load_sann
Train SANN:             http://127.0.0.1:5020/train_sann

========================================================================

If loading for the first time, with a new dataset:

Navigate to the following URL (one after another) after startup:

1) http://127.0.0.1:5020/train_autoencoder

    Response should be: {'status': 'success', 'message': 'Training complete'}

2) http://127.0.0.1:5020/evaluate_autoencoder

    Response should be: {'status': 'success', 'message': 'Evaluation complete. Pixelwise accuracy (image #8): 98.64%'}

3) http://127.0.0.1:5020/train_sann

    Response should be: {'status': 'success', 'message': 'SANN initialization complete'}

========================================================================

'''

from flask import Flask,request,jsonify
from flask_cors import CORS,cross_origin
from datetime import datetime
import os
import pprint
import numpy as np
pp = pprint.PrettyPrinter(indent=4)
import base64

# =====================================================================================

from functions import *

# =====================================================================================

global state, autoencoder, encoder, decoder, sann, x_train, y_train, x_encoded

request_history = []
autoencoder = None
encoder = None
decoder = None
x_train = None
x_test = None

state = {
    "input": b64_encode(np.zeros((28, 28))),
    "classification": {
        "key" : 0,
        "label" : 0,
        "confidence" : 0
    },
    "reverse_salience" : 0,
    "prediction": b64_encode(np.zeros((28, 28))),
    "request_load" : 0
}

# =====================================================================================

app=Flask(__name__)
CORS(app, support_credentials=True)
@cross_origin(supports_credentials=True)

# =====================================================================================

@app.route('/', methods=['GET'])
def index():
    return "Cortex server running!"

# =====================================================================================

def update_request_load():
    global state, request_history
    request_history = cleanup_log_array(request_history)
    state["request_load"] = len(request_history)

# =====================================================================================

@app.route("/process_image", methods=['GET'])
def process_image(img_b64_decoded=""):

    log("Cortex: /process_image")

    global state, autoencoder, encoder, decoder, sann, x_train, x_test, request_history

    # Decode input image
    state["input"] = request.args.get('img_b64_decoded')
    img = b64_decode(state["input"])

    # Resize Image (required for SANN)
    img_small = reshape_resize_reshape(img)

    # Prediction
    encoded_imgs, decoded_imgs = reconstruct(encoder, decoder, [[img]])
    state["prediction"] = b64_encode(decoded_imgs.reshape((28,28)))

    # Classification (class) & Reverse Salience
    img_class, img_confidence, reverse_salience = classify_from_encoding(sann, encoded_imgs[0])
    state["classification"]["key"] = str(img_class)
    state["classification"]["confidence"] = str(round(img_confidence,3))
    state["reverse_salience"] = str(round(reverse_salience*reverse_salience*5000,1))

    # Trigger arousal system
    if (float(state["reverse_salience"]) > 10):
        trigger_chemical_release(chemical="noradrenaline", value=state["reverse_salience"])

    # Classification (label)
    img_label = lookup_label(img_class)
    state["classification"]["label"] = img_label

    # Add request to history
    request_history.append([datetime.now()])

    update_request_load()

    return return_json(state)

# =====================================================================================

@app.route("/current_state", methods=['GET'])
def current_state():

    #log("Cortex: /current_state")

    global state

    update_request_load()

    return return_json(state)

# =====================================================================================

@app.route("/train_autoencoder", methods=['GET'])
def train_autoencoder():

    log("Cortex: /train_autoencoder")

    global state, autoencoder, encoder, decoder, x_train, y_train, x_encoded

    # Initialize NN
    log ("Initializing NN models...")
    autoencoder, encoder, decoder = init_nn(input_dim = len(x_train[0]))

    log ("Training NN...")
    autoencoder, history = train_nn(autoencoder, x_train, x_train, epochs_n=100)

    log ("Saving NN models...")
    save_nn_to_file(autoencoder, "autoencoder")

    log ("Generating encoded values...")
    x_encoded, _ = reconstruct(encoder, decoder, x_train)

    log ("Training complete")

    return return_json({"status": "success", "message" : "Training complete"})

# =====================================================================================

@app.route("/evaluate_autoencoder", methods=['GET'])
def evaluate_autoencoder():

    log("Cortex: /evaluate_autoencoder")

    global state, autoencoder, encoder, decoder, x_train, x_encoded

    try:

        log ("Evaluating dataset...")
        x_encoded, decoded_imgs = reconstruct(encoder, decoder, x_train)
        log ("Showing original image...")
        original_image = x_train[8].reshape((28,28))
        show_image(original_image)
        log ("Showing encoding...")
        show_image(x_encoded[8].reshape((4,4)))
        log ("Showing decoded image...")
        decoded_image = decoded_imgs[8].reshape((28,28))
        show_image(decoded_image)
        log ("Calculating accuracy...")
        pixelwise_error = sum(sum(abs(decoded_image - original_image))) / sum(sum(original_image))
        pixelwise_accuracy = 100 * (1 - pixelwise_error)
        log ("Pixelwise accuracy (image #8): %.2f%%" % pixelwise_accuracy)
        log ("Evaluation complete")

    except:

        log("Failed to evaluate autoencoder model. Try /train or /load first.")
        return return_json({"status": "error", "message" : "Failed to evaluate model. Try /train or /load first."})

    return return_json({"status": "success", "message" : "Evaluation complete. Pixelwise accuracy (image #8): %.2f%%" % pixelwise_accuracy})

# =====================================================================================

@app.route("/load_dataset", methods=['GET'])
def load_dataset():

    log("Cortex: /load_dataset")

    global x_train, y_train

    try:

        log ("Loading dataset...")
        x_train, y_train = load_training_images(log_polar_transform=True)

    except:

        log("Failed to load dataset.")
        return return_json({"status": "error", "message" : "Failed to load dataset."})

    return return_json({"status": "success", "message" : "Loading dataset complete"})

# =====================================================================================

@app.route("/load_autoencoder", methods=['GET'])
def load_autoencoder():

    log("Cortex: /load_autoencoder")

    global state, autoencoder, encoder, decoder, x_train, y_train, x_encoded

    try:

        # Initialize NN
        log ("Initializing Autoencoder model...")
        autoencoder, encoder, decoder = init_nn(input_dim = len(x_train[0]))

        log ("Loading Autoencoder weights...")
        autoencoder = load_nn_from_file(autoencoder, "autoencoder")

        log ("Loading encoded values of dataset...")
        x_encoded, _ = reconstruct(encoder, decoder, x_train)

    except:

        log("Failed to load Autoencoder model from file. Try /train first.")
        return return_json({"status": "error", "message" : "Failed to load Autoencoder model from file. Try /train first."})

    return return_json({"status": "success", "message" : "Loading autoencoder model complete"})

# =====================================================================================

@app.route("/load_sann", methods=['GET'])
def load_sann():

    log("Cortex: /load_sann")

    global state, sann, x_train, y_train, x_encoded

    try:

        # Initialize NN
        log ("Initializing SANN model...")
        input_dim = 4
        nn_dim = [input_dim*input_dim, input_dim*input_dim, 3]
        sann = SANN(nn_dim)
        sann.set_activation_function(sann.activation_sigmoid)

        log ("Loading SANN weights (.bin files)...")
        sann_filename = "saved_nn_models/model_sann.bin"
        sann = SANN.load_from_bin(sann_filename)

        log ("Tagging image image #9 (dog) with high salience...")
        sann.initialize_salience()
        sann.salience_train_activation(x_encoded[8].reshape(4*4), salience_value=1)

    except:

        log("Failed to load SANN model from file.")
        return return_json({"status": "error", "message" : "Failed to load SANN model from file."})

    return return_json({"status": "success", "message" : "SANN initialization complete"})

# =====================================================================================

@app.route("/train_sann", methods=['GET'])
def train_sann():

    log("Cortex: /train_sann")

    global state, sann, x_train, y_train, x_encoded

    # Initialize NN
    log ("Initializing SANN model...")
    input_dim = 4
    nn_dim = [input_dim*input_dim, input_dim*input_dim, 3]
    sann = SANN(nn_dim)
    sann.set_activation_function(sann.activation_sigmoid)

    log ("Training SANN...")
    sann.initialize_salience()
    sann.train(x_encoded, y_train, x_encoded, y_train, epochs=200)

    log ("Saving SANN model (bin format)...")
    sann_filename = "saved_nn_models/model_sann.bin"
    sann.save_to_bin(sann_filename)

    log ("Training complete")

    return return_json({"status": "success", "message" : "SANN initialization complete"})

# =====================================================================================

@app.route("/cls")
def cls():
    os.system('cls')
    return ""

# =====================================================================================

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()

@app.route('/shutdown')
def shutdown():
    shutdown_server()
    return 'Server shutting down...'

# =====================================================================================

cls()
load_dataset()
load_autoencoder()
load_sann()
log ("Starting Cortex System: %s" % str(datetime.now()), level="INFO", color="ERR.")
app.run(host=host, port=ports["cortex"], debug=True, threaded=True)

# =====================================================================================

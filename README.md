# A distributed brain-inspired cognitive architecture written in Python

## About

Author: Leendert A Remmelzwaal
Written: May 2020
License: CC BY

## Description

A distributed brain-inspired cognitive architecture written in Python

## Environment Setup

1) Install Python 3.6.4 or later: https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe) (25Mb)

2) Install Miniconda (this is to make use of highly optimized C++ implementation of NN training): https://docs.conda.io/en/latest/miniconda.html

3) Open Miniconda command prompt and install dependencies:

```
conda install numpy scipy mkl-service libpython m2w64-toolchain
```

4) Run: `venv_setup.bat` to setup Virtual Env with PIP dependencies

## Starting the Flask Servers

There are 3 Python Flask servers.

To start all 3 servers together:

```
start_servers.bat
```

To start each server individually:

(1) Arousal System

```
venv_activate.bat
cls && python server_arousal.1.0.py
```

(2) Thalamus

```
venv_activate.bat
cls && python server_thalamus.1.0.py
```

(3) Cortex

```
venv_activate.bat
cls && python server_cortex.1.0.py
```

## Front-end GUI

To start the front-end GUI interface:

```
venv_activate.bat
cls && python main.py
```

## API Calls

### Arousal System:

Current state: 		http://127.0.0.1:5010/current_state
Add chemical: 		http://127.0.0.1:5000/add?chemical=noradrenaline&value=100

### Thalamus:

Current state: 		http://127.0.0.1:5010/current_state
Process image: 		http://127.0.0.1:5010/process_image?img_b64_decoded=ABC

### Cortex:

Current state:          http://127.0.0.1:5020/current_state
Process image:          http://127.0.0.1:5020/process_image?img_b64_decoded=ABC
Load Autoencoder:       http://127.0.0.1:5020/load_autoencoder
Train Autoencoder:      http://127.0.0.1:5020/train_autoencoder
Evaluate Autoencoder:   http://127.0.0.1:5020/evaluate_autoencoder
Load SANN:              http://127.0.0.1:5020/load_sann
Train SANN:             http://127.0.0.1:5020/train_sann

'''
THALAMUS SYSTEM

Current state: http://127.0.0.1:5010/current_state
Process image: http://127.0.0.1:5010/process_image?img_b64_decoded=""

'''

from flask import Flask,request,jsonify
from flask_cors import CORS,cross_origin
from datetime import datetime
import os
import pprint
import numpy as np
pp = pprint.PrettyPrinter(indent=4)
from functions import *

# =====================================================================================

global state

state = {
    "threshold": {
        "timestamp": datetime.now(),
        "value": 0
    },
    "difference": {
        "timestamp": datetime.now(),
        "value": 0
    },
    "context": {
        "value": empty_img(),
        "timestamp": datetime.now()
    },
    "input": empty_img(),
    }

threshold_range = {"min":0, "max":40}

# =====================================================================================

def update_threshold():

    global state

    # Get Noradrenaline Level
    noradrenaline_level = int(get_current_state(port_name="arousal")["noradrenaline"]["value"])

    # Calculate threshold (in range)
    threshold_level = threshold_range["min"] + (1 - noradrenaline_level / 100) * (threshold_range["max"] - threshold_range["min"])

    # Update state
    state["threshold"]["value"] = int(threshold_level) / 300
    state["threshold"]["timestamp"] = datetime.now()

# =====================================================================================

app=Flask(__name__)
CORS(app, support_credentials=True)
@cross_origin(supports_credentials=True)

# =====================================================================================

@app.route('/', methods=['GET'])
def index():
    return "Thalamic server running!"

# =====================================================================================

@app.route("/current_state", methods=['GET'])
def current_state():

    #log("Thalamus: /current_state")

    global state

    update_threshold()

    return return_json(state)

# =====================================================================================

@app.route("/process_image", methods=['GET'])
def process_image(img_b64_decoded=""):

    log("Thalamus: /process_image")

    global state

    update_threshold()

    # Decode input image
    state["input"] = request.args.get('img_b64_decoded')
    img = b64_decode(state["input"]).reshape((28,28))

    # Calculate error / diff
    context = b64_decode("%s" % state["context"]["value"]).reshape((28,28))
    image_difference = rmsdiff(img, context)
    state["difference"]["value"] = str(image_difference)
    state["difference"]["timestamp"] = datetime.now()

    # Compare input to prediction
    if (image_difference > state["threshold"]["value"]):

        cortex_state = send_to(port_name="cortex", img=img)

        # Prediction
        prediction_img = b64_decode(cortex_state['prediction']).reshape((28,28))
        state["context"]["value"] = b64_encode(prediction_img)
        state["context"]["timestamp"] = datetime.now()

    return return_json(state)

# =====================================================================================

@app.route("/cls")
def cls():
    os.system('cls')
    return ""

# =====================================================================================

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()

@app.route('/shutdown')
def shutdown():
    shutdown_server()
    return 'Server shutting down...'

# =====================================================================================

cls()
log ("Starting Thalamus System: %s" % str(datetime.now()), level="INFO", color="BLUE")
app.run(host=host, port=ports["thalamus"], debug=True, threaded=True)

# =====================================================================================

# https://stackoverflow.com/questions/7624765/converting-an-opencv-image-to-black-and-white

import cv2
import numpy as np
import imutils
import matplotlib.pyplot as plt
import time

from keras.layers import Input, Dense
from keras.models import Sequential, Model
from keras.datasets import mnist, fashion_mnist, cifar10
from keras.models import model_from_json, load_model
from keras.optimizers import Adam

from PIL import ImageFont, ImageDraw, Image
import requests
from dateutil import parser
import pytz

from functions import *

# ==========================================
# Euclidean Images
# ==========================================

trainX_euclidean, trainY_euclidean = load_training_images(log_polar_transform=False)
trainX_euclidean = trainX_euclidean.reshape((-1,28,28))

'''
show_image(trainX[0])
show_image(trainX[4])
show_image(trainX[8])
'''

# ==========================================
# Log polar transformed images
# ==========================================

trainX, trainY = load_training_images(log_polar_transform=True)
trainX = trainX.reshape((-1,28,28))

'''
show_image(trainX[0])
show_image(trainX[4])
show_image(trainX[8])
'''

# ==========================================
# Inverse log polar transform
# ==========================================

'''
show_image(transform_logpolar_inv(trainX[0]))
show_image(transform_logpolar_inv(trainX[4]))
show_image(transform_logpolar_inv(trainX[8]))
'''

# ==========================================

# Select starting image
img = trainX[0]

# Resize window
width = 500
info_panel_height = 350
height = width + info_panel_height
slider_bar_height = 44

cv2.namedWindow('GUI', cv2.WINDOW_NORMAL)
cv2.createTrackbar('Image','GUI', 0, 11, nothing)

img_brain = cv2.imread("img/brain_modules.png", cv2.IMREAD_GRAYSCALE) / 255.0

# ==========================================================================

plot_len = 20
labels = [str(i-plot_len) for i in range(0,plot_len)]
noradrenaline_level_history = [0 for i in range(0,plot_len)]
thalamus_threshold_history = [0 for i in range(0,plot_len)]
thalamus_difference_history = [0 for i in range(0,plot_len)]
cortex_load_history = [0 for i in range(0,plot_len)]
reverse_salience_history = [0 for i in range(0,plot_len)]
thalamus_send_history = [0 for i in range(0,plot_len)]

# ==========================================================================

while(True):

    # Calculate Thresholds
    key = cv2.getTrackbarPos('Image','GUI');

    # ==========================================================================

    # Prepare Image (Euclidean)
    img_euclidean = trainX_euclidean[key]

    # Prepare image for GUI
    input_img_euclidean = img_euclidean.copy()
    input_img_euclidean = 1 - imutils.resize(input_img_euclidean, width=width)

    # ==========================================================================

    # Prepare Image (Log Polar)
    img = trainX[key]

    # Prepare image for GUI
    input_img = img.copy()
    input_img = imutils.resize(input_img, width=width)

    # ==========================================================================
    # Thalamus

    # Send input image to Thalamus
    thalamus_state = send_to(port_name="thalamus", img=img)

    # Show prediction
    prediction_img = b64_decode(thalamus_state['context']['value']).reshape((28,28))
    prediction = imutils.resize(prediction_img, width=width)

    # Show time since last cortical prediction
    prediction_last_updated = parser.parse(thalamus_state['context']['timestamp']).replace(tzinfo=pytz.UTC)
    time_now = datetime.now().replace(tzinfo=pytz.UTC)
    time_since_last_updated = min(10,(time_now - prediction_last_updated).total_seconds())

    # Log polar inverse transform (GUI)
    prediction_euclidean = 1 - transform_logpolar_inv(prediction_img)
    prediction_euclidean = imutils.resize(prediction_euclidean, width=width)

    # ==========================================================================
    # Cortex

    # Classification
    cortex_state = get_current_state(port_name="cortex")
    class_number = cortex_state["classification"]["key"]
    class_label = cortex_state["classification"]["label"]
    class_confidence = round(float(cortex_state["classification"]["confidence"])*100)

    # Reverse Salience
    reverse_salience = round(float(cortex_state["reverse_salience"]),1)

    # ==========================================================================
    # Update parameters
    # ==========================================================================

    noradrenaline_level = int(get_current_state(port_name="arousal")["noradrenaline"]["value"])
    thalamus_state = get_current_state(port_name="thalamus")
    thalamus_threshold = int(thalamus_state["threshold"]["value"] * 300)
    thalamus_difference = int(float(thalamus_state["difference"]["value"]) * 300)
    cortex_load = 10 * int(100 * get_current_state(port_name="cortex")["request_load"] / 150)
    desire_to_act = 0

    # ==========================================================================
    # Update history
    # ==========================================================================

    noradrenaline_level_history.append(noradrenaline_level)
    noradrenaline_level_history = noradrenaline_level_history[-plot_len:]

    thalamus_difference_history.append(thalamus_difference)
    thalamus_difference_history = thalamus_difference_history[-plot_len:]

    thalamus_send_history.append(50 if thalamus_difference > thalamus_threshold else 0)
    thalamus_send_history = thalamus_send_history[-plot_len:]

    thalamus_threshold_history.append(thalamus_threshold)
    thalamus_threshold_history = thalamus_threshold_history[-plot_len:]

    cortex_load_history.append(cortex_load)
    cortex_load_history = cortex_load_history[-plot_len:]

    reverse_salience_history.append(reverse_salience)
    reverse_salience_history = reverse_salience_history[-plot_len:]

    # ==========================================================================
    # Chart
    # ==========================================================================

    fig = plt.figure(figsize=(12, 3))

    plt.subplot(141)
    plt.title('Thalamus')
    plt.xticks([])
    plt.xlabel('Time')
    plt.ylim((0, 100))
    plt.bar(labels, thalamus_send_history, color="#CCCCFF", linestyle="dashed", label='Send to cortex?')
    plt.plot(labels, thalamus_threshold_history, color="#0000FF", marker=".", label='Thalamus Threshold')
    plt.plot(labels, thalamus_difference_history, color="#0000FF", linestyle="dashed", label='Prediction Difference')
    plt.legend()

    plt.subplot(142)
    plt.title('Arousal System')
    plt.xticks([])
    plt.xlabel('Time')
    plt.ylim((0, 100))
    plt.plot(labels, noradrenaline_level_history, color="#FF0000", marker=".", label='Noradrenaline Level')
    plt.legend()

    plt.subplot(143)
    plt.title('Prefrontal Cortex')
    plt.xticks([])
    plt.xlabel('Time')
    plt.ylim((0, 100))
    plt.bar(labels, thalamus_send_history, color="#99FF99", linestyle="dashed", label='Image received?')
    plt.plot(labels, cortex_load_history, color="#00FF00", marker=".", label='Cortex Load')
    plt.plot(labels, reverse_salience_history, color="#FF66FF", marker=".", label='Reverse Salience')
    plt.legend()

    plt.subplot(144)
    plt.title('Motor Cortex')
    plt.xticks([])
    plt.xlabel('Time')
    plt.ylim((0, 100))
    plt.plot(labels, noradrenaline_level_history, color="#FF9900", marker=".", label='Desire to Act')
    plt.legend()

    plt.tight_layout(pad=1)

    # Convert plt to numpy array
    chart_width = 1200
    chart_img = get_img_from_fig(fig)
    chart_img_small = imutils.resize(chart_img, width=chart_width)
    (chart_h, chart_w) = chart_img_small.shape[0:2]
    #plt.savefig('img/temp.png')

    plt.close("all")

    # ==========================================================================
    # GUI
    # ==========================================================================

    # Combining Images (row_1)
    img_row1 = np.zeros((info_panel_height, width*2 + 510))

    # Combining Images (row_2) - black line
    img_row2 = np.zeros((5, width*2 + 510))

    # Combining Images (row_3)
    img_line = np.zeros((width, 5))
    img_row3 = np.concatenate((input_img_euclidean, img_line, prediction_euclidean, img_line, img_brain), axis=1)

    # Combining Images (row 1 & row 2)
    img_combined = np.concatenate((img_row1, img_row2, img_row3), axis=0)

    # ==========================================================================
    # Generate Image
    fontSize = 16
    pil_im = Image.fromarray(img_combined)
    draw = ImageDraw.Draw(pil_im)
    font = ImageFont.truetype("font\\DroidSansMono.ttf", fontSize)

    # Values
    text_template = "%16s: %4s"
    draw.text((30, 30), text_template % ("Image #", key), font=font)
    draw.text((30, 60), text_template % ("Threshold", thalamus_threshold), font=font)
    draw.text((30, 90), text_template % ("Difference", thalamus_difference), font=font)
    draw.text((30,120), text_template % ("Noradrenaline", noradrenaline_level), font=font)
    draw.text((30,150), text_template % ("Reverse Salience", reverse_salience), font=font)
    draw.text((30,180), text_template % ("Cortex Load", "%s%%"%cortex_load), font=font)
    draw.text((30,210), text_template % ("Desire to Act", desire_to_act), font=font)
    draw.text((30,240), text_template % ("Class", class_number), font=font)
    draw.text((30,270), text_template % ("Label", class_label), font=font)
    draw.text((30,300), text_template % ("Confidence", "%s%%"%class_confidence), font=font)

    # Bars
    '''
    text_template = "%s"
    draw.text((560, 60), text_template % (gui_bars(thalamus_threshold)), font=font)
    draw.text((560, 90), text_template % (gui_bars(thalamus_difference)), font=font)
    draw.text((560,120), text_template % (gui_bars(noradrenaline_level)), font=font)
    draw.text((560,150), text_template % (gui_bars(reverse_salience)), font=font)
    draw.text((560,180), text_template % (gui_bars(cortex_load, max_bars=10)), font=font)
    draw.text((560,210), text_template % (gui_bars(desire_to_act)), font=font)
    draw.text((560,300), text_template % (gui_bars(class_confidence)), font=font)
    '''

    # Labels
    text_template = "%s"
    draw.text((30,390), text_template % ("Sensory Input (Euclidean)"), font=font, fill="#000")
    draw.text((540,390), text_template % ("Cortical Prediction (Euclidean)"), font=font, fill="#000")

    # Convert back to np array
    img_combined = np.array(pil_im)

    # ==========================================================================
    # Convert image to colour
    img_combined_rgb = cv2.cvtColor(np.array(img_combined * 255, dtype = np.uint8), cv2.COLOR_GRAY2RGB)

    # Add chart
    y_start = int(info_panel_height/2) - int(chart_h/2)
    y_end = y_start + chart_h
    x_start = img_row1.shape[1] - chart_width - 20
    x_end = img_row1.shape[1] - 20
    img_combined_rgb[y_start:y_end, x_start:x_end] = chart_img_small

    # Resize and show
    window_height, window_width = img_combined_rgb.shape[:2]
    cv2.resizeWindow('GUI', window_width, window_height)
    cv2.imshow('GUI', img_combined_rgb)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    # ==========================================================================

    if (time_since_last_updated < 1):

        # Add ellipse (cortex)
        center_coordinates = (1140, 480-280+info_panel_height)
        axesLength = (70, 48)
        angle = 0
        startAngle = 0
        endAngle = 360
        color = (0, 255, 0)
        thickness = 5

        img_combined_rgb = cv2.ellipse(img_combined_rgb, center_coordinates, axesLength, angle, startAngle, endAngle, color, thickness)

    if (True):

        # Add ellipse (thalamus)
        center_coordinates = (1213, 609-280+info_panel_height)
        axesLength = (58, 33)
        angle = 0
        startAngle = 0
        endAngle = 360
        color = (0, 0, 255)
        thickness = 5

        img_combined_rgb = cv2.ellipse(img_combined_rgb, center_coordinates, axesLength, angle, startAngle, endAngle, color, thickness)

    if (True):

        # Add ellipse (sensory inputs)
        center_coordinates = (1090, 687-280+info_panel_height)
        axesLength = (54, 33)
        angle = 0
        startAngle = 0
        endAngle = 360
        color = (0, 255, 255)
        thickness = 5

        img_combined_rgb = cv2.ellipse(img_combined_rgb, center_coordinates, axesLength, angle, startAngle, endAngle, color, thickness)

    if (noradrenaline_level > 10):

        # Add ellipse (noradrenaline inputs)
        center_coordinates = (1363, 623-280+info_panel_height)
        axesLength = (70, 40)
        angle = 0
        startAngle = 0
        endAngle = 360
        color = (230, 230, 0)
        thickness = 5

        img_combined_rgb = cv2.ellipse(img_combined_rgb, center_coordinates, axesLength, angle, startAngle, endAngle, color, thickness)

    # ==========================================================================

    time.sleep(0.1)

    # Resize and show
    window_height, window_width = img_combined_rgb.shape[:2]
    cv2.resizeWindow('GUI', window_width, window_height)
    cv2.imshow('GUI', img_combined_rgb)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    time.sleep(0.1)

    # ==========================================================================

cv2.release()
cv2.destroyAllWindows()

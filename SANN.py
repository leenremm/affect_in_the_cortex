# ==============================================================================

# Author: LA Remmelzwaal
# Date: January 2020
# Adapted from: https://bitbucket.org/leenremm/python_salience_affected_neural_network/src/master/

import os
os.environ["KERAS_BACKEND"] = "theano"
import numpy as np
np.random.seed(0)
import pickle
import json
import cv2
from keras.utils import to_categorical
from matplotlib import pyplot as plt

class SANN:

    '''
    Class for training deep neural networks with backpropagation algorithm.

    Constructor: initialise neural network with array of layer sizes.
                 i.e. [2, 3, 2] means 2 input neurons, 3 hidden and 2 output neurons
    '''
    def __init__(self, layer_sizes):
        self.n_layers = len(layer_sizes)
        self.layer_sizes = layer_sizes
        self.init_method = self.init_rand
        self.activation_function = SANN.activation_sigmoid

        # w = wights, y = outputs, g = gradients
        self.w = []
        self.y = []
        self.g = []

        # average error, learning speed, error change limit
        self.avg_error = None
        self.speed = 0.1
        self.variation = None

        # input layer
        self.w.append(None)
        # other layers
        # (we need to extend each layer by one neuron so that bias weight plays nicely with matrix multiplication)
        i = 1
        while i < self.n_layers:
            layer = np.zeros((self.layer_sizes[i-1]+1, self.layer_sizes[i]+1))
            layer[0][0] = 1.0
            self.w.append(layer)
            i += 1

    '''
    Set network learning speed. Usually low positive float number lower than 1.
    '''
    def set_speed(self, speed):
        self.speed = speed

    def get_speed(self):
        return self.speed

    '''
    Initialize salience.
    '''

    def initialize_output(self, x_train):
        # initialise output matrices
        sample_shape = (1, x_train.shape[1]+1)
        self.y = []
        self.y.append(None)
        i = 1
        while i < self.n_layers:
            shape_b = self.w[i].shape
            shape_c = (sample_shape[0], shape_b[1])
            self.y.append(np.zeros(shape_c))
            i += 1

    def initialize_salience(self):
        self.salience_speed = 1
        # initialise node salience matrices
        self.s = []
        for i in self.layer_sizes:
            self.s.append(np.zeros(i))

    '''
    Set salience learning speed. Usually low positive float number lower than 1.
    '''

    def set_salience_speed(self, salience_speed):
        self.salience_speed = salience_speed

    def get_salience_speed(self):
        return self.salience_speed

    def set_variation(self, variation):
        self.variation = variation

    def get_variation(self):
        try:
            return self.variation
        except:
            return None

    '''
    Set which weight initialisation method to use in neural network.
    options: init_rand, init_heetal, init_static
    '''
    def set_init_method(self, method):
        self.init_method = method

    '''
    Random initialisation of weights. Weights are random but near 0.
    '''
    def init_rand(self):
        delta = 0.1
        i = 1
        while i < self.n_layers:
            d = self.w[i].shape
            y = 0
            while y < d[0]:
                x = 1
                while x < d[1]:
                    self.w[i][y][x] = np.random.uniform(-delta, delta)
                    x += 1
                y += 1
            i += 1

    '''
    He-et-al initialization.
    '''
    def init_heetal(self):
        i = 1
        while i < self.n_layers:
            d = self.w[i].shape
            y = 0
            while y < d[0]:
                x = 1
                while x < d[1]:
                    self.w[i][y][x] = np.random.normal(0.0, 0.5)*np.sqrt(2.0/self.layer_sizes[i-1])
                    x += 1
                y += 1
            i += 1

    '''
    Set all weight to 0.5.
    '''
    def init_static(self):
        i = 1
        while i < self.n_layers:
            d = self.w[i].shape
            y = 0
            while y < d[0]:
                x = 1
                while x < d[1]:
                    self.w[i][y][x] = 0.5
                    x += 1
                y += 1
            i += 1

    '''
    Set which activation function to use in network.
    options: activation_sigmoid, activation_tanh
    '''
    def set_activation_function(self, function):
        self.activation_function = function
        pass

    '''
    Apply activation function to all weights in layer.
    '''
    def _apply_activation(self, matrix, node_salience):
        d = matrix.shape
        y = 0
        while y < d[0]:
            x = 1
            while x < d[1]:
                matrix[y][x] = self.activation_function(matrix[y][x], node_salience[0] * self.salience_speed, self.variation) # Salience matrix index 0 applies to all iterations
                x += 1
            y += 1

    '''
    Sigmoid activation function.
    '''
    @staticmethod
    def activation_sigmoid(x, salience_value, variation):

        if (variation == "gradient"):
            return 1.0 / (1.0 + np.exp(-1.0*(x * np.sqrt( (0.5)**(-salience_value) ))))      # Gradient Change
        elif (variation == "offset_positive"):
            return 1.0 / (1.0 + np.exp(-1.0*(x + salience_value)))                         # Horizontal Offset
        elif (variation == "offset_negative"):
            return 1.0 / (1.0 + np.exp(-1.0*(x - salience_value)))                         # Horizontal Offset
        elif (variation == "amplitude"):
            return np.sqrt((0.5)**(-salience_value)) / (1.0 + np.exp(-1.0*(x)))            # Amplitude
        else:
            return 1.0 / (1.0 + np.exp(-1.0*(x)))                                           # Standard sigmoidal fn

        #return np.sqrt((0.5)**(-salience_value)) / (1.0 + np.exp(-1.0*(np.sqrt((0.5)**(-salience_value)))*(x)))            # Combined: Gradient Change + Amplitude

    '''
    Hyperbolic tangent activation function.
    '''
    @staticmethod
    def activation_tanh(x):
        return (1.0 - np.exp(-x)) / (1.0 + np.exp(-x))

    '''
    Train matrix of one or more training samples and return average error at the end of training.
    '''
    def train(self, x_train, y_train, x_test, y_test, epochs=1000):
        if x_train.shape[0] != y_train.shape[0]:
            raise Exception("Number of rows in samples matrix must be equal to number of rows in desired matrix.")
        if y_train.shape[1] != self.layer_sizes[-1]:
            raise Exception("Number of columns in desired matrix must be the same as size of output layer (%d)." % self.layer_sizes[-1])

        # initialise neural network
        self.init_method()

        # prepare input list and desired list
        samples = []
        sample_shape = (1, x_train.shape[1]+1)
        i = 0
        while i < x_train.shape[0]:
            # sample, desired
            s = np.array([np.concatenate(([-1.0], x_train[i]))])
            d = np.array(y_train[i])
            samples.append((s, d))
            i += 1

        # Start training (epochs)
        n_samples = len(samples)
        for epoch in range(0,epochs):

            # reset output matrices
            self.y = []
            self.y.append(None)
            i = 1
            while i < self.n_layers:
                shape_b = self.w[i].shape
                shape_c = (sample_shape[0], shape_b[1])
                self.y.append(np.zeros(shape_c))
                i += 1

            # reset gradient matrices
            self.g = []
            self.g.append(None)
            i = 1
            while i < self.n_layers:
                self.g.append(np.zeros(self.layer_sizes[i]))
                i += 1

            # Shuffle Samples
            np.random.shuffle(samples)
            avg_error = 0.0
            i_sample = 1

            # Loop through samples
            for sample in samples:
                self._feedforward(sample[0])
                self._backprop(sample)
                avg_error += self._mse(sample)
                avg_error_samples = avg_error/float(i_sample)
                print("Epoch: %3d/%3d, Sample: %3d/%3d, Error: %8.5f" % (epoch+1, epochs, i_sample, n_samples, avg_error_samples), end="\r")
                i_sample += 1

            # return average error
            self.avg_error = avg_error_samples

            # Evaluate (training and test sets)
            acc_train = self.evaluate(x_train, y_train) * 100
            acc_test = self.evaluate(x_test, y_test) * 100

            # Print Accuracies to screen
            print("Epoch: %3d/%3d, Sample: %3d/%3d, Error: %8.5f, Acc (train): %5.2f%%, Acc (test): %5.2f%%" % (epoch+1, epochs, i_sample-1, n_samples, avg_error_samples, acc_train, acc_test))

        self.y = None
        self.g = None

        return avg_error_samples

    '''
    Calculate feed forward part of backpropagation algorithm and save outputs to list y
    '''
    def _feedforward(self, sample_features):
        try:
            self.y[0] = sample_features
            i = 1
            while i < self.n_layers:
                np.matmul(self.y[i-1], self.w[i], self.y[i])
                self._apply_activation(self.y[i], self.s[i])
                i += 1
        except:
            print ("Feedforward error")
            pass
    '''
    Do backpropagation part. Calculate gradients and fix weights.
    '''
    def _backprop(self, sample):
        desired = sample[1]

        # gradients and weights in output layer
        l1 = self.n_layers-1
        i = 1
        while i <= self.layer_sizes[l1]:
            l0 = l1-1

            o = self.y[l1][0][i]
            g = o * (1.0 - o) * (desired[i-1] - o)
            self.g[l1][i-1] = g

            # fix weights of that neuron
            j = 0
            while j <= self.layer_sizes[l0]:
                self.w[l1][j][i] = self.w[l1][j][i] + self.speed * self.g[l1][i-1] * self.y[l0][0][j]
                j += 1

            i += 1

        # gradients and weight in all other layers
        # l1 = current, l0 previous, l2 = next
        l1 -= 1
        while l1 > 0:
            l2 = l1+1
            l0 = l1-1

            # for each neuron
            i = 1
            while i <= self.layer_sizes[l1]:

                # for each weight from that neuron
                dp = 0.0
                j = 1
                while j <= self.layer_sizes[l2]:
                    dp += self.g[l2][j-1] * self.w[l2][i][j]
                    j += 1

                # calculate gradient
                y = self.y[l1][0][i]
                g = y * (1.0 - y) * dp
                self.g[l1][i-1] = g

                # fix weights of that neuron
                j = 0
                while j <= self.layer_sizes[l0]:
                    self.w[l1][j][i] = self.w[l1][j][i] + self.speed*self.g[l1][i-1]*self.y[l0][0][j]
                    j += 1

                i += 1

            l1 -= 1

    '''
    Calculate mean square error of sample output. Call this after backprop phase.
    '''
    def _mse(self, sample):
        desired = sample[1]
        o = self.y[-1][0]
        l = len(desired)
        i = 0
        mse = 0.0
        while i < l:
            mse += np.power(desired[i] - o[i+1], 2.0)
            i += 1
        return mse

    '''
    Calculate root mean square error of sample output. Call this after backprop phase.
    '''
    def _rmse(self, sample):
        return np.sqrt(self._mse(sample))

    '''
    Get average error of trained network.
    '''
    def get_avg_error(self):
        return self.avg_error


    '''
    Calculate output of one or more samples and return it.

    x_train:
    [
        [x11, x12, x13, ..., x1N],
        [x21, x22, x23, ..., x2N],
        ...
    ]
    '''
    def solve(self, x_train):

        samples = np.zeros((x_train.shape[0], x_train.shape[1]+1))

        i = 0
        while i < x_train.shape[0]:
            samples[i][0] = -1.0
            j = 1
            while j <= x_train.shape[1]:
                samples[i][j] = x_train[i][j-1]
                j += 1
            i += 1

        # initialise output matrices
        self.y = []
        self.y.append(None)
        i = 1
        while i < self.n_layers:
            shape_b = self.w[i].shape
            shape_c = (x_train.shape[0], shape_b[1])
            self.y.append(np.zeros(shape_c))
            i += 1

        self._feedforward(samples)
        o = self.y[-1][:,1:]

        self.y = None
        self.g = None
        return o

    '''
    Return the accuracy of a NN
    '''
    def evaluate(self, x_train, y_train):
        o = self.solve(x_train)
        o_max = np.argmax(o, axis=1)
        y_max = np.argmax(y_train, axis=1)
        accuracy = np.average(o_max == y_max)
        return accuracy

    '''
    Save network weights and important settings to binary file, so you do not have to train it every time.
    '''
    def save_to_bin(self, file):
        with open(file, 'wb') as output:
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)

    '''
    Load already trained network from binary file.
    '''
    @staticmethod
    def load_from_bin(file):
        network = None # type: SANN
        with open(file, 'rb') as input:
            network = pickle.load(input)
        return network

    '''
    Save network weights and important settings to json file.
    (Binary file is more appropriate unless you want to send trained network to someone who does not have this class.)
    '''
    def save_to_json(self, file):
        w = []
        w.append(self.w[0])
        i = 1
        while i < len(self.w):
            w.append(self.w[i].tolist())
            i += 1

        dump = {
            "layer_sizes": self.layer_sizes,
            "avg_error": self.avg_error,
            "speed": self.speed,
            "weights": w
        }

        with open(file, 'w') as f:
            json.dump(dump, f, indent=4)

    '''
    Load network weights and some settings from json file.
    (Binary file is more appropriate unless you want to send trained network to someone who does not have this class.)
    '''
    @staticmethod
    def load_from_json(file):
        f = open(file)
        dump = json.load(f)

        network = SANN(dump["layer_sizes"])
        network.avg_error = dump["avg_error"]
        network.speed = dump["speed"]

        dump_w = dump["weights"]
        w = []
        w.append(dump_w[0])
        i = 1
        l = len(dump_w)
        while i < l:
            w.append(np.array(dump_w[i]))
            i += 1

        network.w = w
        return network

    # ===========================================================================
    # Custom SANN Methods
    # ===========================================================================

    def show_100_images(img_arr, samples=100):
        fig=plt.figure(figsize=(6, 6))
        rows = 10
        columns = int(samples / rows)
        for i in range(0, columns*rows):
            img = img_arr[i]
            dim = int(np.sqrt(img.size))
            img = img.reshape((dim, dim))
            fig.add_subplot(rows, columns, i+1)
            plt.axis('off')
            plt.imshow(img, cmap='gray', vmin=0, vmax=1)
        plt.show()

    def grayscale(array):
        return_array = []
        for img in array:
            return_array.append(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY))
        return np.array(return_array, "uint8")

    def resize(array, dim=28):
        return_array = []
        for img in array:
            resized_img = cv2.resize(img, (dim, dim))
            return_array.append(resized_img)
        return np.array(return_array)

    def load_dataset(dataset, input_dim, samples=0):

        if (dataset == "xor"):
            # xor problem
            x_train = np.array([
                [0.0, 0.0],
                [0.0, 1.0],
                [1.0, 0.0],
                [1.0, 1.0]
            ])
            y_train = np.array([
                [0.0],
                [1.0],
                [1.0],
                [0.0]
            ])
            y_train = to_categorical(y_train)
            return x_train, y_train
        elif (dataset == "cifar10"):
            from keras.datasets import cifar10 as dataset
            (x_train, y_train), (_, _) = dataset.load_data()
            # Grayscale
            x_train = SANN.grayscale(x_train)
        elif (dataset == "fashion_mnist"):
            from keras.datasets import fashion_mnist as dataset
            (x_train, y_train), (_, _) = dataset.load_data()
        elif (dataset == "mnist"):
            from keras.datasets import mnist as dataset
            (x_train, y_train), (_, _) = dataset.load_data()
        else:
            quit()

        # Limit the size of the dataset
        if (samples > 0 and samples < len(x_train)):
            x_train = x_train[:samples]
            y_train = y_train[:samples]

        # Resize x_train
        x_train = x_train.astype('float32') / 255.
        x_train = SANN.resize(x_train, input_dim)
        x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))

        # Resize y_train
        y_train = y_train.reshape((len(x_train), 1))
        y_train = to_categorical(y_train)

        # Return
        return x_train, y_train

    # ===========================================================================
    # Salience Methods
    # ===========================================================================

    '''
    Apply salience to each node in the network
    '''
    def salience_train_activation(self, sample, salience_value=1):

        # Initial internal variables
        self.initialize_output(np.array([sample,sample]))

        sample_prep = [np.concatenate(([-1.0], sample))]

        # Feedforward first before applying salience
        self._feedforward(sample_prep)

        # Loop through all nodes
        for n, layer_size in enumerate(self.layer_sizes):

            if n == 0: continue # Skip the input nodes

            #relative_activation = self.y[n][0] / max(self.y[n][0])

            for m in range(layer_size):

                # Get salience and activation values
                node_s_old = self.s[n][m]
                node_activation = self.y[n][0][m+1]

                # Update Salience
                node_s_change = (1 - node_s_old) * node_activation * salience_value # Close the gap
                node_s_new = self.s[n][m] + node_s_change
                self.s[n][m] = node_s_new

                # Logging
                '''
                print ("%d-%03d:" % (n, m))
                print ("node salience (old): %.4f," % (node_s_old))
                print ("salience active?  %d," % (salience_value))
                print ("salience speed:  %.4f," % (self.salience_speed))
                print ("node activation: %.4f," % node_activation)
                print ("node offset (change): %.4f," % node_s_change)
                print ("node offset (new): %.4f," % node_s_new)
                print ("======================================")
                '''

    '''
    Apply salience: Update weights of connection in the network, relative to parent node activation
    '''
    def salience_update_weights(self, sample):

        # Initial internal variables
        self.initialize_output(np.array([sample,sample]))

        sample_prep = [np.concatenate(([-1.0], sample))]

        # Feedforward first before applying salience
        self._feedforward(sample_prep)

        # Loop through all layers
        for n, layer_size in enumerate(self.layer_sizes):

            if n == 0: continue # Skip the input nodes

            # Layer 2 and 3:
            # Loop through all nodes, and update pre-synaptic weights
            for m in range(layer_size):

                # Get salience and activation values
                node_s = self.s[n][m]
                node_activation = self.y[n][0][m+1]

                # Loop through all pre-synaptic weights
                num_weights = len(self.w[n]) - 1
                for o in range(1,num_weights+1):
                    weight_old = self.w[n][o][m+1]
                    weight_change_factor = 1 + abs(node_s * node_activation * self.salience_speed)
                    weight_new = weight_old * weight_change_factor
                    self.w[n][o][m+1] = weight_new
                    # Logging
                    # print ("%d-%d-%d: %.6f, %.6f, %.6f" % (n, m, o, weight_old, weight_change_factor, weight_new))

    '''
    Reverse Salience Signal from each node in the network
    '''
    def calculate_reverse_salience(self, sample):

        # Feedforward first before applying salience
        self.initialize_output(np.array([sample,sample]))
        sample_prep = [np.concatenate(([-1.0], sample))]
        self._feedforward(sample_prep)

        # Loop through all nodes
        layer_salience_arr = [[],[],[]]
        network_salience_arr = [[],[],[]]
        for n, layer_size in enumerate(self.layer_sizes):

            if n == 0: continue    # Skip input layer (salience = 0)
            if n == 1: continue    # Skip input layer (salience = 0)

            node_salience_relative = self.s[n] / np.max(self.s[n])

            for m in range(layer_size):

                node_activation = self.y[n][0][m+1]
                node_salience = node_salience_relative[m] ** 2

                # Node salience
                node_reverse_salience = node_activation * node_salience

                # Update layer salience
                layer_salience_arr[n].append(node_reverse_salience)

            # Update network salience
            network_salience_arr[n] = (np.average(layer_salience_arr[n]))

        # Network average salience

        network_average = np.average(network_salience_arr[n])

        return network_average

    def int_to_hex(self, nr):
        h = format(int(nr), 'x')
        return '0' + h if len(h) % 2 else h

    def draw(self, w=None, w2=None):

        if (w == None):
            w = self.w

        # https://gist.github.com/craffel/2d727968c3aaebd10359

        max_nodes_in_layers = 6

        left=.1
        right=.9
        bottom=.1
        top=.9

        fig = plt.figure(figsize=(6, 6))
        ax = fig.gca()
        ax.axis('off')

        layer_sizes = [min(max_nodes_in_layers,i) for i in self.layer_sizes]

        n_layers = len(layer_sizes)
        v_spacing = (top - bottom)/float(max(layer_sizes))
        h_spacing = (right - left)/float(len(layer_sizes) - 1)

        # Max salience value (used for colour)
        try:
            max_s = max([max(i) for i in abs(self.s)])
        except:
            max_s = 1

        # Nodes
        for n, layer_size in enumerate(layer_sizes):
            #print(n, layer_size)
            layer_top = v_spacing*(layer_size - 1)/2. + (top + bottom)/2.
            for m in range(layer_size):
                if n == 0:
                    line_weight = 0.5
                    fill_colour = "w"
                else:
                    try:
                        s_hex = self.int_to_hex(255 - int(255 * abs(self.s[n][m]) / max_s))
                        if (self.s[n][m] > 0):
                            fill_colour = "#%sff%s" % (s_hex,s_hex)
                        else:
                            fill_colour = "#ff%s%s" % (s_hex,s_hex)
                    except:
                        line_weight = 0.5
                        fill_colour = "w"
                circle = plt.Circle((n*h_spacing + left, layer_top - m*v_spacing), v_spacing/4.,
                                    color=fill_colour, ec='k', zorder=4, linewidth=0.5)
                ax.add_artist(circle)

        # Edges
        for n, (layer_size_a, layer_size_b) in enumerate(zip(layer_sizes[:-1], layer_sizes[1:])):
            layer_top_a = v_spacing*(layer_size_a - 1)/2. + (top + bottom)/2.
            layer_top_b = v_spacing*(layer_size_b - 1)/2. + (top + bottom)/2.
            for m in range(layer_size_a):
                for o in range(layer_size_b):
                    line_weight = w[n+1][m+1][o+1] * 2
                    '''
                    try:
                        print (n,m,o,w[n+1][m+1][o+1],w2[n+1][m+1][o+1],w2[n+1][m+1][o+1] - w[n+1][m+1][o+1])
                    except:
                        print (n,m,o,w[n+1][m+1][o+1])
                    '''
                    if (w2 == None):
                        line_weight = w[n+1][m+1][o+1] * 2
                    else:
                        line_weight = (w2[n+1][m+1][o+1] - w[n+1][m+1][o+1]) * 2
                    line_colour = "g" if (line_weight > 0) else "r"
                    start_x = n*h_spacing + left
                    start_y = (n + 1)*h_spacing + left
                    end_x = layer_top_a - m*v_spacing
                    end_y = layer_top_b - o*v_spacing
                    #print (start_x,start_y,end_x,end_y)
                    line = plt.Line2D([start_x, start_y], [end_x, end_y], c=line_colour, linewidth=abs(line_weight))
                    ax.add_artist(line)
        plt.show()

# ==============================================================================

set batdir=%~dp0
set anacondir="C:\Users\leenr\Anaconda3\Scripts"

start cmd /k "%anacondir%\activate.bat && cd %batdir% && venv_activate.bat && call python server_arousal.1.0.py"
start cmd /k "%anacondir%\activate.bat && cd %batdir% && venv_activate.bat && call python server_thalamus.1.0.py"
start cmd /k "%anacondir%\activate.bat && cd %batdir% && venv_activate.bat && call python server_cortex.1.0.py"
PAUSE
start cmd /k "%anacondir%\activate.bat && cd %batdir% && venv_activate.bat && call python main.py"
